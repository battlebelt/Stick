'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stick = function () {
    function Stick(element, options) {
        _classCallCheck(this, Stick);

        this.menu = document.querySelector(element);
        this.options = options || {
            offset: 0
        };
        this.elementPos = this.menu.getBoundingClientRect().top + this.options.offset;
        this.scrollValue = 0;
        this.tickUpdate();
    }

    _createClass(Stick, [{
        key: 'scroll',
        value: function scroll(direction) {
            // touch top or not
            this.topCheck();

            // have to hide or not
            /*if(this.scrollValue <= window.pageYOffset && window.pageYOffset > this.options.offset/2) {
                this.menu.classList.add('hide-elem');
            } else {
                this.menu.classList.remove('hide-elem');
            }*/

            if (direction === "down") {
                this.menu.classList.add('hide-elem');
            } else {
                this.menu.classList.remove('hide-elem');
            }
        }
    }, {
        key: 'topCheck',
        value: function topCheck() {
            if (window.pageYOffset >= this.elementPos) {
                this.menu.classList.add('top');
            } else {
                this.menu.classList.remove('top');
            }
        }
    }, {
        key: 'scrollUpdate',
        value: function scrollUpdate(direction) {
            this.scroll(direction);
        }
    }, {
        key: 'tickUpdate',
        value: function tickUpdate() {
            var appear = this;
            var ticker = setInterval(function () {
                appear.topCheck.call(appear);
            }, 1000);
        }
    }]);

    return Stick;
}();