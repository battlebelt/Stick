# Appear

## Use
To use, load the Appear script (es5 and es6 available) in your application.

### Javascript part

`var nav = new Stick('.nav-element')` to create a new instance of the plugin.
`nav.scrollUpdate()` have to be placed in a scroll event callback in order to detect the position of the element (advice: use your scroll event with some bubbling or throttle system if you don't want to fire the callback too many times).  
**The plugin let you handle the scroll event by yourself**

#### Options (simply add them in the same html class attribute after the primary plugin class)

##### Offset

`var nav = new Stick('.nav-element', {
        offset: 100
})`

## Here's an exemple with throttling.

~~~
var nav = new Appear('.nav-element'),
    ticking = false;

/*
* scroll event handling with throttle
*/

function scrollUpdate() {
    if(!ticking) {
        window.requestAnimationFrame(function() {
            nav.scrollUpdate();
            ticking = true;
        });
    }
    ticking = false;
};

window.addEventListener('scroll', scrollUpdate, false);
~~~
