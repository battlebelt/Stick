class Stick {
    constructor(element, options) {
        this.menu = document.querySelector(element);
        this.options = options || {
            offset: 0
        };
        this.elementPos = this.menu.getBoundingClientRect().top + this.options.offset;
        this.scrollValue = 0;
        this.tickUpdate();
    }

    scroll(direction) {
        // touch top or not
        this.topCheck();

        // have to hide or not
        /*if(this.scrollValue <= window.pageYOffset && window.pageYOffset > this.options.offset/2) {
            this.menu.classList.add('hide-elem');
        } else {
            this.menu.classList.remove('hide-elem');
        }*/

        if(direction === "down") {
            this.menu.classList.add('hide-elem');
        } else {
            this.menu.classList.remove('hide-elem');
        }
    }

    topCheck() {
        if(window.pageYOffset >= this.elementPos) {
            this.menu.classList.add('top');
        } else {
            this.menu.classList.remove('top');
        }
    }

    scrollUpdate(direction) {
        this.scroll(direction);
    }

    tickUpdate() {
        var appear = this;
        var ticker = setInterval(function() {
            appear.topCheck.call(appear)
        }, 1000);
    }
}